#Security group to allow only specific IP to SSH
resource "aws_security_group" "app_server_sg" {
    vpc_id = aws_vpc.dev_vpc.id
    name   = "app_server_sg" 
    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.public_subnet_cird}"]     # ["0.0.0.0/0"] allow only for public subnet via admin instance
    }

    egress {
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        cidr_blocks = ["${var.private_subnet_cird}"] # access only for private subnet range
     }

     ingress {
        from_port = 80 
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"] # http access allowed globally
     }

    tags =  {
        Name = "app-server-sg"
    }
}
#Security group to allow only traffic from public instance to DB
resource "aws_security_group" "db_server_sg" {
    vpc_id = aws_vpc.dev_vpc.id
    name   = "db_server_sg"
    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        cidr_blocks = ["${var.public_subnet_cird}"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.public_subnet_cird}"]
    }    

    tags =  {
        Name = "db_server_sg"
    }
}
resource "aws_security_group" "admin_server_sg" {
    name = "admin_server_sg"
    description = "security group for admin instance"

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]  # should be allow only for Persoanl IP range 
    }
    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.vpc_cidr}"] #allow ssh access inside VPC
    }
    egress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }

        egress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"] 
    }

    vpc_id = aws_vpc.dev_vpc.id

    tags = {
        Name = "admin_server_sg"
    }
}
