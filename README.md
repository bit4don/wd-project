# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Before you begin ##
Before you run create a IAM user called "admin" policy can be found in the repository as iam_policy.txt 

Create a public and private key in AWS and place the private key in cloned terraform directory as admin.pem and set permission to 600

Place your AWS access and secret keys in vars.tf 

## Run terraform ##

terraform init
terraform plan
terraform apply


Terraform will deploy 3 instances 
   
        1 admin server 
        2 wordpress server
        3 database server

admin server is to run ansible scripts ones terraform infrastrcture is created to deploy wordpress and mysql database

wordpress server is to host wordpress 

database server is to host mysql databse 

only admin server have SSH allowed if you need to access wordpress or databse server you need to ssh from admin server to private ip address of wordpress or database server . 

private key is available in /home/ec2-user/.ssh/admin.pem in admin server you can ssh like eg. ssh -i admin.pem ec2-user@20.0.1.109

ones the deployment is finished you should be able to access wordpress site in browser with public IP of wordpress instance  . eg 52.18.153.146/wordpress  

all IP addresses publish ones deployemnt complete as a terraform output in shell


##Troubleshoot 
If you get this below error might be a time out issue , please run the terraform apply again
 
eg.  Error: error executing "/tmp/terraform_1615469828.sh": wait: remote command exited without exit status or exit signal 




