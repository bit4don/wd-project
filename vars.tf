variable "aws_access_key" {
  default = "xxxxx"
}

variable "aws_secret_key" {
  default = "xxxxxx"
}

variable "aws_region" {
  default = "eu-west-1"
}

variable "vpc_cidr" {
  default = "20.0.0.0/16"
}

variable "public_subnet_cird" {
  default = "20.0.1.0/24"
}

variable "public_av_zone" {
  default = "eu-west-1a"
}

variable "private_subnet_cird" {
  default = "20.0.2.0/24"
}

variable "private_av_zone" {
  default = "eu-west-1b"
}

#variable "key_path" {
#    default = "/home/project/vpc/admin_key"
#}
variable "key_name" {
    default = "admin"
}
