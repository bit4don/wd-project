#Create  Public Subnet
resource "aws_subnet" "dev-public-subnet" {
    vpc_id = aws_vpc.dev_vpc.id
    cidr_block = var.public_subnet_cird
    map_public_ip_on_launch = "true"
    availability_zone = var.public_av_zone
    tags = {
        Name = "dev-public-subnet"
    }
}
#Create Private Subnet
resource "aws_subnet" "dev-private-subnet" {
    vpc_id = aws_vpc.dev_vpc.id
    cidr_block = var.private_subnet_cird
    availability_zone = var.private_av_zone
    tags = {
        Name = "dev-private-subnet"
    }
}
#Assign elastic IP to admin instance
resource "aws_eip" "admin_eip" {
    vpc = true
}

#Assign elastic IP to wordpress server
resource "aws_eip" "wordpress_eip" {
    instance = aws_instance.web_server.id
    vpc = true
}

#Create Internet GateWay
resource "aws_internet_gateway" "dev-igw" {
    vpc_id = aws_vpc.dev_vpc.id
    tags =  {
        Name = "dev-igw"
    }
}

#create routing enrty for  internet gateway 
resource "aws_route_table" "dev-public-route-table" {
    vpc_id = aws_vpc.dev_vpc.id
    route {
        cidr_block = "0.0.0.0/0" 
        gateway_id = aws_internet_gateway.dev-igw.id 
    }
    
    tags = {
        Name = "dev-public-route-table"
    }
}

#Accociate public subnet to publlic routing table
resource "aws_route_table_association" "dev-public-subnet-association"{
    subnet_id = aws_subnet.dev-public-subnet.id
    route_table_id = aws_route_table.dev-public-route-table.id
}

resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.admin_eip.id
  subnet_id     = aws_subnet.dev-public-subnet.id

  tags = {
    Name = "private-nat-gw"
  }
}

##Create routing entry for private network to connect internet
resource "aws_route_table" "dev-private-route-table" {
    vpc_id = aws_vpc.dev_vpc.id

    route {
        cidr_block  = "0.0.0.0/0"
        gateway_id  = aws_nat_gateway.nat_gw.id
    }

    tags =  {
        Name = "dev-private-route-table"
    }
}

##Accociate private routing table to admin instance
resource "aws_route_table_association" "dev-private-subnet-association" {
    subnet_id = aws_subnet.dev-private-subnet.id
    route_table_id = aws_route_table.dev-private-route-table.id
}
