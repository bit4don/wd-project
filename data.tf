data "aws_ami" "ami" {
 # executable_users = ["self"]
  most_recent      = true
#  name_regex       = "^Amazon Linux AMI 2\\d{3}"
  owners           = ["amazon"]

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name  = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]

}

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
