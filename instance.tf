resource "aws_instance" "admin_server" {
    ami = data.aws_ami.ami.id
    instance_type = "t2.micro"
    key_name = var.key_name
    vpc_security_group_ids = ["${aws_security_group.admin_server_sg.id}"]
    subnet_id = aws_subnet.dev-public-subnet.id
    associate_public_ip_address = true
    source_dest_check = false
    tags = {
        Name = "admin-server"
    }

    connection {
     type     = "ssh"
     user     = "ec2-user"
     host     = self.public_ip
     private_key  = file("admin.pem")
     agent = false
}
     provisioner "remote-exec" {
     inline = [
     "sudo  amazon-linux-extras install ansible2 -y"
     ]
 }

    provisioner "file" {
    source      = "admin.pem"
    destination = "/home/ec2-user/.ssh/admin.pem"
  }

    provisioner "remote-exec" {
    inline = [
    "chmod 600 /home/ec2-user/.ssh/admin.pem",
    "mkdir /home/ec2-user/ansible_scripts"
     ]  
}

    provisioner "file" { 
    source      = "ansible_scripts"
    destination = "/home/ec2-user/"
  }

    provisioner "file" {
    source      = "ansible.cfg"
    destination = "/home/ec2-user/ansible.cfg"
  }

    provisioner "remote-exec" {
    inline = [
    "sudo mv /home/ec2-user/ansible.cfg /etc/ansible/"
     ]
 }

    provisioner "remote-exec" {
    inline = [
    "ansible-playbook  -i '${aws_instance.db_server.private_ip},' /home/ec2-user/ansible_scripts/db/mysql_db.yml"
     ]
}

    provisioner "remote-exec" {
    inline = [
     "echo   private_ip: ${aws_instance.db_server.private_ip}>> /home/ec2-user/ansible_scripts/app/vars.yml"
     ]
 }

     provisioner "remote-exec" {
     inline = [
     "ansible-playbook  -i '${aws_instance.web_server.private_ip},' /home/ec2-user/ansible_scripts/app/wordpress.yml"
     ]
 }
 
}


resource "aws_instance" "web_server" {
    ami = data.aws_ami.ami.id 
    instance_type = "t2.micro"
    # VPC
    subnet_id = aws_subnet.dev-public-subnet.id
    # Security Group
    vpc_security_group_ids = ["${aws_security_group.app_server_sg.id}"]
    # the Public SSH key
    key_name = var.key_name
    tags = {
    Name   = "wordpress-server"
  }
}


resource "aws_instance" "db_server" {
    ami = data.aws_ami.ami.id
    instance_type = "t2.micro"
    # VPC
    subnet_id = aws_subnet.dev-private-subnet.id
    # Security Group
    vpc_security_group_ids = ["${aws_security_group.db_server_sg.id}"]
    # the Public SSH key
    key_name =  var.key_name
    tags = {
    Name   = "wordpress-db-server"
  }

}
