output "wordpress_server_public_IP" {
  value = aws_instance.web_server.public_ip
}

output "databse_server_private_IP" {
  value = aws_instance.db_server.private_ip
}

output "admin_server_private_IP" {
  value = aws_instance.admin_server.private_ip
}
